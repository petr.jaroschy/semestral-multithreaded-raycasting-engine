﻿namespace _3dEngineRendering
{
	partial class MainFrameWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.chbFisheyeEnable = new System.Windows.Forms.CheckBox();
            this.btnFloorColor = new System.Windows.Forms.Button();
            this.btnSkyColor = new System.Windows.Forms.Button();
            this.btnSetRotSpeed = new System.Windows.Forms.Button();
            this.btnSetFOV = new System.Windows.Forms.Button();
            this.RotateSpeedLabel = new System.Windows.Forms.Label();
            this.FOVlabel = new System.Windows.Forms.Label();
            this.tbRotSpeed = new System.Windows.Forms.TextBox();
            this.tbFOV = new System.Windows.Forms.TextBox();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnLoadMap = new System.Windows.Forms.Button();
            this.btnStartTasks = new System.Windows.Forms.Button();
            this.btnStartMulti = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRenderOne = new System.Windows.Forms.Button();
            this.LeftRotationTimer = new System.Windows.Forms.Timer(this.components);
            this.RightRotationTimer = new System.Windows.Forms.Timer(this.components);
            this.BackwardMovementTimer = new System.Windows.Forms.Timer(this.components);
            this.ForwardMovementTimer = new System.Windows.Forms.Timer(this.components);
            this.tbCores = new System.Windows.Forms.TextBox();
            this.lblCoreAmount = new System.Windows.Forms.Label();
            this.btnSetCores = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chbFisheyeEnable
            // 
            this.chbFisheyeEnable.AutoSize = true;
            this.chbFisheyeEnable.Checked = true;
            this.chbFisheyeEnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbFisheyeEnable.Location = new System.Drawing.Point(18, 265);
            this.chbFisheyeEnable.Name = "chbFisheyeEnable";
            this.chbFisheyeEnable.Size = new System.Drawing.Size(113, 17);
            this.chbFisheyeEnable.TabIndex = 33;
            this.chbFisheyeEnable.Text = "Fisheye Correction";
            this.chbFisheyeEnable.UseVisualStyleBackColor = true;
            this.chbFisheyeEnable.CheckedChanged += new System.EventHandler(this.chbFishEyeEnable_CheckedChanged);
            // 
            // btnFloorColor
            // 
            this.btnFloorColor.Location = new System.Drawing.Point(15, 236);
            this.btnFloorColor.Name = "btnFloorColor";
            this.btnFloorColor.Size = new System.Drawing.Size(75, 23);
            this.btnFloorColor.TabIndex = 31;
            this.btnFloorColor.Text = "Floor Color";
            this.btnFloorColor.UseVisualStyleBackColor = true;
            this.btnFloorColor.Click += new System.EventHandler(this.btnFloorColor_Click);
            this.btnFloorColor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnFloorColor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnSkyColor
            // 
            this.btnSkyColor.Location = new System.Drawing.Point(15, 207);
            this.btnSkyColor.Name = "btnSkyColor";
            this.btnSkyColor.Size = new System.Drawing.Size(75, 23);
            this.btnSkyColor.TabIndex = 29;
            this.btnSkyColor.Text = "Sky Color";
            this.btnSkyColor.UseVisualStyleBackColor = true;
            this.btnSkyColor.Click += new System.EventHandler(this.btnSkyColor_Click);
            this.btnSkyColor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnSkyColor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnSetRotSpeed
            // 
            this.btnSetRotSpeed.Enabled = false;
            this.btnSetRotSpeed.Location = new System.Drawing.Point(121, 62);
            this.btnSetRotSpeed.Name = "btnSetRotSpeed";
            this.btnSetRotSpeed.Size = new System.Drawing.Size(50, 23);
            this.btnSetRotSpeed.TabIndex = 28;
            this.btnSetRotSpeed.Text = "Set";
            this.btnSetRotSpeed.UseVisualStyleBackColor = true;
            this.btnSetRotSpeed.Click += new System.EventHandler(this.btnSetRotSpeed_Click);
            this.btnSetRotSpeed.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnSetRotSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnSetFOV
            // 
            this.btnSetFOV.Enabled = false;
            this.btnSetFOV.Location = new System.Drawing.Point(121, 23);
            this.btnSetFOV.Name = "btnSetFOV";
            this.btnSetFOV.Size = new System.Drawing.Size(50, 23);
            this.btnSetFOV.TabIndex = 27;
            this.btnSetFOV.Text = "Set";
            this.btnSetFOV.UseVisualStyleBackColor = true;
            this.btnSetFOV.Click += new System.EventHandler(this.btnSetFOV_Click);
            this.btnSetFOV.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnSetFOV.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // RotateSpeedLabel
            // 
            this.RotateSpeedLabel.AutoSize = true;
            this.RotateSpeedLabel.Location = new System.Drawing.Point(12, 48);
            this.RotateSpeedLabel.Name = "RotateSpeedLabel";
            this.RotateSpeedLabel.Size = new System.Drawing.Size(81, 13);
            this.RotateSpeedLabel.TabIndex = 26;
            this.RotateSpeedLabel.Text = "Rotating Speed";
            // 
            // FOVlabel
            // 
            this.FOVlabel.AutoSize = true;
            this.FOVlabel.Location = new System.Drawing.Point(12, 9);
            this.FOVlabel.Name = "FOVlabel";
            this.FOVlabel.Size = new System.Drawing.Size(86, 13);
            this.FOVlabel.TabIndex = 25;
            this.FOVlabel.Text = "FOV - in degrees";
            // 
            // tbRotSpeed
            // 
            this.tbRotSpeed.Location = new System.Drawing.Point(15, 64);
            this.tbRotSpeed.Name = "tbRotSpeed";
            this.tbRotSpeed.Size = new System.Drawing.Size(100, 20);
            this.tbRotSpeed.TabIndex = 24;
            this.tbRotSpeed.Text = "0.02";
            this.tbRotSpeed.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.tbRotSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // tbFOV
            // 
            this.tbFOV.Location = new System.Drawing.Point(15, 25);
            this.tbFOV.Name = "tbFOV";
            this.tbFOV.Size = new System.Drawing.Size(100, 20);
            this.tbFOV.TabIndex = 23;
            this.tbFOV.Text = "70";
            this.tbFOV.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.tbFOV.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnUp
            // 
            this.btnUp.Enabled = false;
            this.btnUp.Location = new System.Drawing.Point(68, 381);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(53, 23);
            this.btnUp.TabIndex = 22;
            this.btnUp.Text = "∧";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            this.btnUp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnUp.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnLeft
            // 
            this.btnLeft.Enabled = false;
            this.btnLeft.Location = new System.Drawing.Point(15, 410);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(50, 23);
            this.btnLeft.TabIndex = 21;
            this.btnLeft.Text = "<";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            this.btnLeft.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnLeft.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnDown
            // 
            this.btnDown.Enabled = false;
            this.btnDown.Location = new System.Drawing.Point(68, 410);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(53, 23);
            this.btnDown.TabIndex = 20;
            this.btnDown.Text = "∨";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            this.btnDown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnDown.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnRight
            // 
            this.btnRight.Enabled = false;
            this.btnRight.Location = new System.Drawing.Point(124, 410);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(50, 23);
            this.btnRight.TabIndex = 19;
            this.btnRight.Text = ">";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            this.btnRight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnRight.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(15, 439);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 18;
            this.btnStart.Text = "Single";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            this.btnStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnStart.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnLoadMap
            // 
            this.btnLoadMap.Location = new System.Drawing.Point(15, 178);
            this.btnLoadMap.Name = "btnLoadMap";
            this.btnLoadMap.Size = new System.Drawing.Size(75, 23);
            this.btnLoadMap.TabIndex = 17;
            this.btnLoadMap.Text = "Load Map";
            this.btnLoadMap.UseVisualStyleBackColor = true;
            this.btnLoadMap.Click += new System.EventHandler(this.btnLoadMap_Click);
            this.btnLoadMap.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnLoadMap.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnStartTasks
            // 
            this.btnStartTasks.Enabled = false;
            this.btnStartTasks.Location = new System.Drawing.Point(15, 497);
            this.btnStartTasks.Name = "btnStartTasks";
            this.btnStartTasks.Size = new System.Drawing.Size(75, 23);
            this.btnStartTasks.TabIndex = 35;
            this.btnStartTasks.Text = "Tasks";
            this.btnStartTasks.UseVisualStyleBackColor = true;
            this.btnStartTasks.Click += new System.EventHandler(this.btnStartTasks_Click);
            this.btnStartTasks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnStartTasks.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnStartMulti
            // 
            this.btnStartMulti.Enabled = false;
            this.btnStartMulti.Location = new System.Drawing.Point(15, 468);
            this.btnStartMulti.Name = "btnStartMulti";
            this.btnStartMulti.Size = new System.Drawing.Size(75, 23);
            this.btnStartMulti.TabIndex = 36;
            this.btnStartMulti.Text = "Multi";
            this.btnStartMulti.UseVisualStyleBackColor = true;
            this.btnStartMulti.Click += new System.EventHandler(this.btnStartMulti_Click);
            this.btnStartMulti.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnStartMulti.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(99, 468);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 37;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            this.btnStop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnStop.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // btnRenderOne
            // 
            this.btnRenderOne.Enabled = false;
            this.btnRenderOne.Location = new System.Drawing.Point(99, 439);
            this.btnRenderOne.Name = "btnRenderOne";
            this.btnRenderOne.Size = new System.Drawing.Size(75, 23);
            this.btnRenderOne.TabIndex = 38;
            this.btnRenderOne.Text = "RenderOne";
            this.btnRenderOne.UseVisualStyleBackColor = true;
            this.btnRenderOne.Click += new System.EventHandler(this.btnRenderOne_Click);
            this.btnRenderOne.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnRenderOne.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // LeftRotationTimer
            // 
            this.LeftRotationTimer.Interval = 15;
            this.LeftRotationTimer.Tick += new System.EventHandler(this.LeftRotationTimer_Tick);
            // 
            // RightRotationTimer
            // 
            this.RightRotationTimer.Interval = 15;
            this.RightRotationTimer.Tick += new System.EventHandler(this.RightRotationTimer_Tick);
            // 
            // BackwardMovementTimer
            // 
            this.BackwardMovementTimer.Interval = 15;
            this.BackwardMovementTimer.Tick += new System.EventHandler(this.BackwardMovementTimer_Tick);
            // 
            // ForwardMovementTimer
            // 
            this.ForwardMovementTimer.Interval = 15;
            this.ForwardMovementTimer.Tick += new System.EventHandler(this.ForwardMovementTimer_Tick);
            // 
            // tbCores
            // 
            this.tbCores.Location = new System.Drawing.Point(15, 104);
            this.tbCores.Name = "tbCores";
            this.tbCores.Size = new System.Drawing.Size(100, 20);
            this.tbCores.TabIndex = 39;
            this.tbCores.Text = "0";
            this.tbCores.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.tbCores.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // lblCoreAmount
            // 
            this.lblCoreAmount.AutoSize = true;
            this.lblCoreAmount.Location = new System.Drawing.Point(12, 88);
            this.lblCoreAmount.Name = "lblCoreAmount";
            this.lblCoreAmount.Size = new System.Drawing.Size(152, 13);
            this.lblCoreAmount.TabIndex = 40;
            this.lblCoreAmount.Text = "Number of cores - (Default = 0)";
            // 
            // btnSetCores
            // 
            this.btnSetCores.Enabled = false;
            this.btnSetCores.Location = new System.Drawing.Point(121, 102);
            this.btnSetCores.Name = "btnSetCores";
            this.btnSetCores.Size = new System.Drawing.Size(50, 23);
            this.btnSetCores.TabIndex = 41;
            this.btnSetCores.Text = "Set";
            this.btnSetCores.UseVisualStyleBackColor = true;
            this.btnSetCores.Click += new System.EventHandler(this.btnSetCores_Click);
            this.btnSetCores.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.btnSetCores.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            // 
            // MainFrameWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1480, 605);
            this.Controls.Add(this.btnSetCores);
            this.Controls.Add(this.lblCoreAmount);
            this.Controls.Add(this.tbCores);
            this.Controls.Add(this.btnRenderOne);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStartMulti);
            this.Controls.Add(this.btnStartTasks);
            this.Controls.Add(this.chbFisheyeEnable);
            this.Controls.Add(this.btnFloorColor);
            this.Controls.Add(this.btnSkyColor);
            this.Controls.Add(this.btnSetRotSpeed);
            this.Controls.Add(this.btnSetFOV);
            this.Controls.Add(this.RotateSpeedLabel);
            this.Controls.Add(this.FOVlabel);
            this.Controls.Add(this.tbRotSpeed);
            this.Controls.Add(this.tbFOV);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnLoadMap);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainFrameWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrameWindow_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFrameWindow_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.CheckBox chbFisheyeEnable;
		private System.Windows.Forms.Button btnFloorColor;
		private System.Windows.Forms.Button btnSkyColor;
		private System.Windows.Forms.Button btnSetRotSpeed;
		private System.Windows.Forms.Button btnSetFOV;
		private System.Windows.Forms.Label RotateSpeedLabel;
		private System.Windows.Forms.Label FOVlabel;
		private System.Windows.Forms.TextBox tbRotSpeed;
		private System.Windows.Forms.TextBox tbFOV;
		private System.Windows.Forms.Button btnUp;
		private System.Windows.Forms.Button btnLeft;
		private System.Windows.Forms.Button btnDown;
		private System.Windows.Forms.Button btnRight;
		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnLoadMap;
		private System.Windows.Forms.Button btnStartTasks;
		private System.Windows.Forms.Button btnStartMulti;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.Button btnRenderOne;
		private System.Windows.Forms.Timer LeftRotationTimer;
		private System.Windows.Forms.Timer RightRotationTimer;
		private System.Windows.Forms.Timer BackwardMovementTimer;
		private System.Windows.Forms.Timer ForwardMovementTimer;
        private System.Windows.Forms.TextBox tbCores;
        private System.Windows.Forms.Label lblCoreAmount;
        private System.Windows.Forms.Button btnSetCores;
    }
}


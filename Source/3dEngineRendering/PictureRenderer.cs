﻿namespace _3dEngineRendering
{
    using System.Drawing;
    using GeometryExtended.DataStructures;


    internal class PictureRenderer
    {
        private readonly Image _currentImage;
        private readonly Graphics _graphics;
        private readonly int _screenWidth;
        private readonly int _screenHeight;

        public PictureRenderer(int screenWidth, int screenHeight, Brush floorBrush, Brush skyBrush)
        {
            _screenWidth = screenWidth;
            _screenHeight = screenHeight;
            FloorBrush = floorBrush;
            SkyBrush = skyBrush;
            _currentImage = new Bitmap(screenWidth, screenHeight);
            _graphics = Graphics.FromImage(_currentImage);
        }

        public Brush FloorBrush { private get; set; }
        public Brush SkyBrush { private get; set; }

        /// <summary>
        /// This method creates a new Image of the screen 
        /// </summary>
        /// <param name="distances">Split DistanceResults from different cores.</param>
        /// <returns>Drawn Image to render on the screen</returns>
        public Image RenderImageFromDistances(DistanceResult[] distances)
        {
            _graphics.FillRectangle(FloorBrush, 0, _screenHeight / 2, _screenWidth, _screenHeight / 2);
            _graphics.FillRectangle(SkyBrush, 0, 0, _screenWidth, _screenHeight / 2);

            int currentLine = 0;
            foreach (var distResult in distances)
            {
                foreach (var distResultResult in distResult.Results)
                {
                    var height = (int)(_screenHeight / distResultResult.Distance);
                    var lineBottom = -height / 2 + _screenHeight / 2;
                    _graphics.DrawLine(distResultResult.Pen, currentLine, lineBottom, currentLine, lineBottom + height);
                    ++currentLine;
                }
            }

            return _currentImage;
        }

        /// <summary>
        /// This method creates a new Image of the screen 
        /// </summary>
        /// <param name="distanceResult">Distance result needed to create a whole screen</param>
        /// <returns>Drawn Image to render on the screen</returns>
        public Image RenderImageFromDistanceResult(DistanceResult distanceResult)
        {
            _graphics.FillRectangle(FloorBrush, 0, _screenHeight / 2, _screenWidth, _screenHeight / 2);
            _graphics.FillRectangle(SkyBrush, 0, 0, _screenWidth, _screenHeight / 2);

            for (int i = 0; i < _screenWidth; i++)
            {
                var height = (int)(_screenHeight / distanceResult[i].Distance);
                var lineBottom = -height / 2 + _screenHeight / 2;
                _graphics.DrawLine(distanceResult[i].Pen, i, lineBottom, i, lineBottom + height);
            }

            return _currentImage;
        }
    }
}

﻿using System.Linq;

namespace _3dEngineRendering
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Threading;
    using System.Windows.Forms;
    using GeometryExtended;
    using GeometryExtended.DataStructures;

    public partial class MainFrameWindow : Form
    {
        private const string MapCouldNotBeLoadedMessage = "Map could not be loaded: ";
        private const string InputErrorMessage3 = "You can't be serious";
        private const string InputErrorMessage2 = "The value is not valid";
        private const string InputErrorMessage1 = "The value cannot be empty";
        private string GameNotLoadedMessage = "Game map is not loaded yet.";
        private int _renderedWidth;
        private int _renderedHeight;
        private Map _gameMap;
        private Player _player;
        private Color _skyColor = Color.DeepSkyBlue;
        private Color _floorColor = Color.Sienna;
        private Thread _renderingThread;
        private RenderingState _renderingState = RenderingState.NotStarted;
        private Graphics _gForm;
        private int? _numberOfCores;

        /// <summary>
        /// Current Distance computer used for current screen and map
        /// </summary>
        private RayCastingDistanceComputer DistanceComputer { get; set; }

        /// <summary>
        /// Current picture renderer used for current screen
        /// </summary>
        private PictureRenderer PictureRenderer { get; set; }

        public MainFrameWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initial setting of graphics and width and height
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            _renderedWidth = Width - 220;
            _renderedHeight = Height - 50;
            _gForm = CreateGraphics();
            _gForm.CompositingMode = CompositingMode.SourceCopy;
            _gForm.InterpolationMode = InterpolationMode.NearestNeighbor;
        }

        /// <summary>
        /// On each resize, some variable need to be recalculated and new instances constructed
        /// </summary>
        private void OnResizing()
        {
            _renderedWidth = Width - 220;
            _renderedHeight = Height - 50;
            if (_renderingState == RenderingState.NotStarted)
            {
                _gForm = CreateGraphics();
                return;
            }

            if (_renderingState != RenderingState.Idle)
            {
                var state = _renderingState;
                btnStop_Click(null, null);
                RenewRendererAndComputer();
                switch (state)
                {
                    case RenderingState.Single:
                        btnStart_Click(null, null);
                        break;
                    case RenderingState.Multi:
                        btnStartMulti_Click(null, null);
                        break;
                    case RenderingState.Task:
                        btnStartTasks_Click(null, null);
                        break;
                }
            }
            else
            {
                RenewRendererAndComputer();
            }
        }

        /// <summary>
        /// Replaces Distance Computer and PictureRenderer by new ones using global fields
        /// </summary>
        private void RenewRendererAndComputer()
        {
            _gForm = CreateGraphics();
            _gForm.CompositingMode = CompositingMode.SourceCopy;
            _gForm.InterpolationMode = InterpolationMode.NearestNeighbor;
            DistanceComputer = new RayCastingDistanceComputer(_gameMap, _renderedWidth, _renderedHeight);
            PictureRenderer = new PictureRenderer(_renderedWidth, _renderedHeight, new SolidBrush(_floorColor),
                new SolidBrush(_skyColor));
        }

        /// <summary>
        /// Creates and Starts a new rendering thread.
        /// </summary>
        private void StartRenderingThread()
        {
            _renderingThread = new Thread(() =>
            {
                while (DistanceComputer.IsComputing)
                {
                    RenderNewFrame();
                }
            });
            _renderingThread.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            OnResizing();
        }

        /// <summary>
        /// Button method for rotating the player right
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRight_Click(object sender, EventArgs e)
        {
            _player.RotateRight();
        }

        /// <summary>
        /// Button method for moving forward
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUp_Click(object sender, EventArgs e)
        {
            if (_gameMap[(int) (_player.CurrentPosition.X + _player.CurrentPosition.DirX / 5),
                (int) (_player.CurrentPosition.Y + _player.CurrentPosition.DirY / 5)].Empty)
            {
                _player.Move(forward: true);
            }
        }

        /// <summary>
        /// Button method for moving backwards
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (_gameMap[(int) (_player.CurrentPosition.X - _player.CurrentPosition.DirX / 5),
                (int) (_player.CurrentPosition.Y - _player.CurrentPosition.DirY / 5)].Empty)
            {
                _player.Move(forward: false);
            }
        }

        /// <summary>
        /// Button method for rotating the player left
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLeft_Click(object sender, EventArgs e)
        {
            _player.RotateLeft();
        }

        /// <summary>
        /// Renders entirely new frame on the form getting image from Picture renderer.
        /// </summary>
        private void RenderNewFrame()
        {
            Image frameBuffer = PictureRenderer.RenderImageFromDistances(DistanceComputer.LastCached.ToArray());
            _gForm.DrawImage(frameBuffer, 190, 5);
        }

        /// <summary>
        /// Button method for choosing a file to load a map from
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoadMap_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.ShowDialog();
            try
            {
                if (fileDialog.CheckFileExists)
                {
                    _gameMap = Parser.ReadMap(fileDialog.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MapCouldNotBeLoadedMessage + ex.Message);
                return;
            }

            _player = new Player(_gameMap.Width / 2d, _gameMap.Height / 2d, 1, 0);
            _player.SetFov(70);
            DistanceComputer = new RayCastingDistanceComputer(_gameMap, _renderedWidth, _renderedWidth);
            PictureRenderer = new PictureRenderer(_renderedWidth, _renderedHeight, new SolidBrush(_floorColor),
                new SolidBrush(_skyColor));
            _renderingState = RenderingState.Idle;

            btnDown.Enabled = true;
            btnLeft.Enabled = true;
            btnRenderOne.Enabled = true;
            btnRight.Enabled = true;
            btnUp.Enabled = true;
            btnStart.Enabled = true;
            btnStartMulti.Enabled = true;
            btnStartTasks.Enabled = true;
            btnStop.Enabled = true;
            btnSetFOV.Enabled = true;
            btnSetRotSpeed.Enabled = true;
            btnSetCores.Enabled = true;
        }

        /// <summary>
        /// Button click method for changing the color of the Sky
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSkyColor_Click(object sender, EventArgs e)
        {
            ColorDialog picker = new ColorDialog();
            picker.ShowDialog();
            _skyColor = picker.Color;
            PictureRenderer.SkyBrush = new SolidBrush(picker.Color);
        }

        /// <summary>
        /// Button click method for changing the color of the Floor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFloorColor_Click(object sender, EventArgs e)
        {
            ColorDialog picker = new ColorDialog();
            picker.ShowDialog();
            _floorColor = picker.Color;
            PictureRenderer.FloorBrush = new SolidBrush(picker.Color);
        }

        private void ChangeStartButtonsEnabled(bool changeTo)
        {
            btnStart.Enabled = changeTo;
            btnStartMulti.Enabled = changeTo;
            btnStartTasks.Enabled = changeTo;
        }

        /// <summary>
        /// Button click method for starting single-threaded computation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (DistanceComputer != null)
            {
                DistanceComputer.StartContinuousComputing(_player);
                Thread.Sleep(100);
                StartRenderingThread();
                _renderingState = RenderingState.Single;
                btnLoadMap.Enabled = false;
                ChangeStartButtonsEnabled(false);
            }
            else
            {
                MessageBox.Show(GameNotLoadedMessage);
            }
        }

        /// <summary>
        /// Button click method for starting manual threads multi-threaded computation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStartMulti_Click(object sender, EventArgs e)
        {
            if (DistanceComputer != null)
            {
                if (_numberOfCores.HasValue)
                {
                    DistanceComputer.StartManualThreadsComputing(_player, _numberOfCores.Value);
                }
                else
                {
                    DistanceComputer.StartManualThreadsComputing(_player);
                }

                StartRenderingThread();
                _renderingState = RenderingState.Multi;
                btnLoadMap.Enabled = false;
                ChangeStartButtonsEnabled(false);
            }
            else
            {
                MessageBox.Show(GameNotLoadedMessage);
            }
        }

        /// <summary>
        /// Button click method for starting task based multi-threaded computation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStartTasks_Click(object sender, EventArgs e)
        {
            if (DistanceComputer != null)
            {
                if (_numberOfCores.HasValue)
                {
                    DistanceComputer.StartTaskComputing(_player, _numberOfCores.Value);
                }
                else
                {
                    DistanceComputer.StartTaskComputing(_player);
                }

                StartRenderingThread();
                _renderingState = RenderingState.Task;
                btnLoadMap.Enabled = false;
                ChangeStartButtonsEnabled(false);
            }
            else
            {
                MessageBox.Show(GameNotLoadedMessage);
            }
        }

        /// <summary>
        /// Button click method for rendering one image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRenderOne_Click(object sender, EventArgs e)
        {
            if (DistanceComputer != null)
            {
                using (Graphics gForm = CreateGraphics())
                {
                    gForm.DrawImage(
                        PictureRenderer.RenderImageFromDistanceResult(
                            DistanceComputer.Compute(_player, ComputingMode.SingleThreading)), 190, 5);
                }
            }
            else
            {
                MessageBox.Show(GameNotLoadedMessage);
            }
        }

        /// <summary>
        /// Button click method for stopping any continuous computation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStop_Click(object sender, EventArgs e)
        {
            if (_renderingState == RenderingState.Idle)
            {
                return;
            }

            if (DistanceComputer != null)
            {
                DistanceComputer.StopAll();
                _renderingThread.Abort();
                _renderingState = RenderingState.Idle;
                btnLoadMap.Enabled = true;
                ChangeStartButtonsEnabled(true);
            }
            else
            {
                MessageBox.Show(GameNotLoadedMessage);
            }
        }

        /// <summary>
        /// Event handling user input for controlling the movement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFrameWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                case Keys.Up:
                    ForwardMovementTimer.Enabled = true;
                    break;
                case Keys.A:
                case Keys.Left:
                    LeftRotationTimer.Enabled = true;
                    break;
                case Keys.S:
                case Keys.Down:
                    BackwardMovementTimer.Enabled = true;
                    break;
                case Keys.D:
                case Keys.Right:
                    RightRotationTimer.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Event handling user input for controlling the movement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFrameWindow_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                case Keys.Up:
                    ForwardMovementTimer.Enabled = false;
                    break;
                case Keys.A:
                case Keys.Left:
                    LeftRotationTimer.Enabled = false;
                    break;
                case Keys.S:
                case Keys.Down:
                    BackwardMovementTimer.Enabled = false;
                    break;
                case Keys.D:
                case Keys.Right:
                    RightRotationTimer.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Timer handling left rotation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LeftRotationTimer_Tick(object sender, EventArgs e)
        {
            _player.RotateLeft();
        }

        /// <summary>
        /// Timer handling right rotation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightRotationTimer_Tick(object sender, EventArgs e)
        {
            _player.RotateRight();
        }

        /// <summary>
        /// Timer handling movement forward
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ForwardMovementTimer_Tick(object sender, EventArgs e)
        {
            if (_gameMap[(int) (_player.CurrentPosition.X + _player.CurrentPosition.DirX / 5),
                (int) (_player.CurrentPosition.Y + _player.CurrentPosition.DirY / 5)].Empty)
            {
                _player.Move(forward: true);
            }
        }

        /// <summary>
        /// Timer handling movement backwards
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackwardMovementTimer_Tick(object sender, EventArgs e)
        {
            if (_gameMap[(int) (_player.CurrentPosition.X - _player.CurrentPosition.DirX / 5),
                (int) (_player.CurrentPosition.Y - _player.CurrentPosition.DirY / 5)].Empty)
            {
                _player.Move(forward: false);
            }
        }

        /// <summary>
        /// if
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFrameWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_renderingState != RenderingState.Idle && _renderingState != RenderingState.NotStarted)
            {
                DistanceComputer.StopAll();
                _renderingThread.Abort();
            }
        }

        /// <summary>
        /// Button for setting FOV of the player
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetFOV_Click(object sender, EventArgs e)
        {
            try
            {
                _player.SetFov(int.Parse(tbFOV.Text));
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show(InputErrorMessage1);
            }
            catch (FormatException)
            {
                MessageBox.Show(InputErrorMessage2);
            }
            catch (OverflowException)
            {
                MessageBox.Show(InputErrorMessage3);
            }
        }

        /// <summary>
        /// Button for setting the rotation speed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetRotSpeed_Click(object sender, EventArgs e)
        {
            try
            {
                _player.SetRotationSpeed(double.Parse(tbRotSpeed.Text));
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show(InputErrorMessage1);
            }
            catch (FormatException)
            {
                MessageBox.Show(InputErrorMessage2);
            }
            catch (OverflowException)
            {
                MessageBox.Show(InputErrorMessage3);
            }
        }

        /// <summary>
        /// Button for setting amount of cores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetCores_Click(object sender, EventArgs e)
        {
            try
            {
                _numberOfCores = int.Parse(tbCores.Text);
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show(InputErrorMessage1);
            }
            catch (FormatException)
            {
                MessageBox.Show(InputErrorMessage2);
            }
            catch (OverflowException)
            {
                MessageBox.Show(InputErrorMessage3);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chbFishEyeEnable_CheckedChanged(object sender, EventArgs e)
        {
            DistanceComputer.CorrectFishEye = chbFisheyeEnable.Checked;
        }
    }

    /// <summary>
    /// Enum for controlling the state of the form/rendering
    /// </summary>
    internal enum RenderingState
    {
        Single,
        Multi,
        Task,
        Idle,
        NotStarted
    }
}

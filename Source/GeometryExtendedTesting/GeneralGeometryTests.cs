﻿namespace GeometryExtendedTesting
{
    using System.Threading;
    using System.Drawing;
    using GeometryExtended;
    using GeometryExtended.DataStructures;
    using NUnit.Framework;

    public class GeneralGeometryTests
    {
        RayCastingDistanceComputer _rayCastingDistanceComputer;
        Player _player;
        Map _gameMap;

        [SetUp]
        public void SetUp()
        {
            Pen samplePen = new Pen(Color.Red);
            Tile[,] tiles = new Tile[4, 4];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    tiles[i, j] = new Tile
                    {
                        Empty = true
                    };
                }
            }
            for (int i = 0; i < 4; i++)
            {
                tiles[0, i] = new Tile
                {
                    Pen = samplePen,
                    SidePen = samplePen
                };
            }
            for (int i = 0; i < 4; i++)
            {
                tiles[3, i] = new Tile
                {
                    Pen = samplePen,
                    SidePen = samplePen
                };
            }
            for (int i = 0; i < 4; i++)
            {
                tiles[i, 0] = new Tile
                {
                    Pen = samplePen,
                    SidePen = samplePen
                };
            }
            for (int i = 0; i < 4; i++)
            {
                tiles[i, 3] = new Tile
                {
                    Pen = samplePen,
                    SidePen = samplePen
                };
            }

            _gameMap = new Map(tiles, 4, 4);
            _player = new Player(2, 2, 1, 0);
            _rayCastingDistanceComputer = new RayCastingDistanceComputer(_gameMap, 20, 15);
        }

        [Test]
        [TestCase(ComputingMode.SingleThreading)]
        [TestCase(ComputingMode.MultiThreading)]
        public void TestWholeComputation(ComputingMode computingMode)
        {
            var result = _rayCastingDistanceComputer.Compute(_player, computingMode);

            for (int i = 0; i < 20; i++)
            {
                Assert.That(result[i].Distance, Is.EqualTo(1), "The result calculated wasn't 1 as predefined");
            }

            _player = new Player(2.5, 2, 1, 0);

            result = _rayCastingDistanceComputer.Compute(_player, computingMode);

            for (int i = 0; i < 20; i++)
            {
                Assert.That(result[i].Distance, Is.EqualTo(0.5), "The result calculated wasn't 1 as predefined");
            }
        }

        [Test]
        public void TestMultiThreadComputation()
        {
            _rayCastingDistanceComputer.StartManualThreadsComputing(_player, 2);
            Thread.Sleep(100);
            _rayCastingDistanceComputer.StopManualThreadsComputing();

            var result = _rayCastingDistanceComputer.LastCached;

            foreach (var item in result)
            {
                foreach (var distance in item.Results)
                {
                    Assert.That(distance.Distance, Is.EqualTo(1), "The result calculated wasn't 1 as predefined");
                }
            }
        }

        [Test]
        public void TestSingleThreadComputation()
        {
            _rayCastingDistanceComputer.StartContinuousComputing(_player);
            Thread.Sleep(100);
            _rayCastingDistanceComputer.StopContinuousComputing();

            var result = _rayCastingDistanceComputer.LastCached;

            foreach (var item in result)
            {
                foreach (var distance in item.Results)
                {
                    Assert.That(distance.Distance, Is.EqualTo(1), "The result calculated wasn't 1 as predefined");
                }
            }
        }

        [Test]
        public void TestTaskComputation()
        {
            _rayCastingDistanceComputer.StartTaskComputing(_player, 2);
            Thread.Sleep(100);
            _rayCastingDistanceComputer.StopTaskComputing();

            var result = _rayCastingDistanceComputer.LastCached;

            foreach (var item in result)
            {
                foreach (var distance in item.Results)
                {
                    Assert.That(distance.Distance, Is.EqualTo(1), "The result calculated wasn't 1 as predefined");
                }
            }
        }
    }
}

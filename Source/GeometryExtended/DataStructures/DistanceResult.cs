﻿namespace GeometryExtended.DataStructures
{
    using System.Drawing;

    public class DistanceResult
    {
        public DistanceResult(int resultWidth)
        {
            Results = new ResultColumn[resultWidth];
        }

        public ResultColumn[] Results { get; set; }

        public ResultColumn this[int x]
        {
            get => Results[x];
            set => Results[x] = value;
        }

        public struct ResultColumn
        {
            public ResultColumn(Pen pen, double distance)
            {
                Pen = pen;
                Distance = distance;
            }

            public Pen Pen { get; }

            public double Distance { get; }
        }
    }
}

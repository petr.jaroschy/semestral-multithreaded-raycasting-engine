﻿using static System.Math;

namespace GeometryExtended.DataStructures
{
    public class Player
    {
        private const double Radian = 57.296;
        public Position CurrentPosition = new Position();
        private double _planeDistance;
        private double _rotateSpeed = 0.02;

        public Player(double x, double y, double dirX, double dirY)
        {
            CurrentPosition.PlaneX = 0;
            CurrentPosition.PlaneY = 0.66;
            CurrentPosition.X = x;
            CurrentPosition.Y = y;
            CurrentPosition.DirX = dirX;
            CurrentPosition.DirY = dirY;
            _planeDistance = (Sqrt(Pow(dirX, 2) + Pow(dirY, 2)));
        }

        /// <summary>
        /// Uses mathematical matrix transform to rotate the plane of the player
        /// </summary>
        public void RotateLeft()
        {
            CurrentPosition = new Position
            {
                X = CurrentPosition.X,
                Y = CurrentPosition.Y,
                DirX = CurrentPosition.DirX * Cos(-_rotateSpeed) - CurrentPosition.DirY * Sin(-_rotateSpeed),
                DirY = CurrentPosition.DirX * Sin(-_rotateSpeed) + CurrentPosition.DirY * Cos(-_rotateSpeed),
                PlaneX = CurrentPosition.PlaneX * Cos(-_rotateSpeed) - CurrentPosition.PlaneY * Sin(-_rotateSpeed),
                PlaneY = CurrentPosition.PlaneX * Sin(-_rotateSpeed) + CurrentPosition.PlaneY * Cos(-_rotateSpeed)
            };
        }

        /// <summary>
        /// Uses mathematical matrix transform to rotate the plane of the player
        /// </summary>
        public void RotateRight()
        {
            CurrentPosition = new Position
            {
                X = CurrentPosition.X,
                Y = CurrentPosition.Y,
                DirX = CurrentPosition.DirX * Cos(_rotateSpeed) - CurrentPosition.DirY * Sin(_rotateSpeed),
                DirY = CurrentPosition.DirX * Sin(_rotateSpeed) + CurrentPosition.DirY * Cos(_rotateSpeed),
                PlaneX = CurrentPosition.PlaneX * Cos(_rotateSpeed) - CurrentPosition.PlaneY * Sin(_rotateSpeed),
                PlaneY = CurrentPosition.PlaneX * Sin(_rotateSpeed) + CurrentPosition.PlaneY * Cos(_rotateSpeed)
            };
        }

        /// <summary>
        /// Recalculates the plane so it matches the FOV given
        /// </summary>
        /// <param name="newFov">New value for Field of View</param>
        public void SetFov(int newFov)
        {
            double planeDistance = 0.66 / Tan(newFov / (2 * Radian));
            double scaling = planeDistance / _planeDistance;
            double newDirX = CurrentPosition.DirX * scaling;
            double newDirY = CurrentPosition.DirY * scaling;
            _planeDistance = (Sqrt(Pow(newDirX, 2) + Pow(newDirY, 2)));
            Position newPosition = new Position
            {
                DirX = newDirX,
                DirY = newDirY,
                PlaneX = CurrentPosition.PlaneX,
                PlaneY = CurrentPosition.PlaneY,
                X = CurrentPosition.X,
                Y = CurrentPosition.Y
            };
            CurrentPosition = newPosition;
        }

        /// <summary>
        /// Sets the new rotation speed, divided by radian constant for future computing
        /// </summary>
        /// <param name="newRotSpeed"></param>
        public void SetRotationSpeed(double newRotSpeed)
        {
            _rotateSpeed = newRotSpeed / Radian;
        }

        /// <summary>
        /// Moves the player in direction of his look
        /// </summary>
        /// <param name="forward"></param>
        public void Move(bool forward)
        {
            if (forward)
            {
                CurrentPosition = new Position
                {
                    PlaneY = CurrentPosition.PlaneY,
                    PlaneX = CurrentPosition.PlaneX,
                    Y = CurrentPosition.Y + 0.05 * CurrentPosition.DirY / _planeDistance,
                    X = CurrentPosition.X + 0.05 * CurrentPosition.DirX / _planeDistance,
                    DirX = CurrentPosition.DirX,
                    DirY = CurrentPosition.DirY
                };
            }
            else
            {
                CurrentPosition = new Position
                {
                    PlaneY = CurrentPosition.PlaneY,
                    PlaneX = CurrentPosition.PlaneX,
                    Y = CurrentPosition.Y - 0.05 * CurrentPosition.DirY / _planeDistance,
                    X = CurrentPosition.X - 0.05 * CurrentPosition.DirX / _planeDistance,
                    DirX = CurrentPosition.DirX,
                    DirY = CurrentPosition.DirY
                };
            }
        }

        public class Position
        {
            public double X;
            public double Y;
            public double DirX;
            public double DirY;
            public double PlaneX;
            public double PlaneY;
        }
    }
}

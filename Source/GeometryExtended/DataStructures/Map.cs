﻿namespace GeometryExtended.DataStructures
{
    public class Map
    {
        private readonly Tile[,] _tiles;

        public Map(Tile[,] tiles, int xSize, int ySize)
        {
            _tiles = tiles;
            Width = xSize;
            Height = ySize;
        }

        public Tile this[int x, int y] => _tiles[x, y];

        public int Height { get; }

        public int Width { get; }
    }
}

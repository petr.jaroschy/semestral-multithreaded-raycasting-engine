﻿namespace GeometryExtended
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using DataStructures;

    /// <summary>
    /// This is computer for Distances for a simple ray-casting engine using CPU.
    /// This computer offers possibility of single or multi-threaded computing along with
    /// more options for continuous computation
    /// </summary>
    public class RayCastingDistanceComputer
    {
        private readonly Map _map;
        private readonly int _availableCores;
        private Thread[] _runningThreads;
        private ComputingState _computingState = ComputingState.Idle;

        /// <summary>
        /// Constructs Ray-casting Distance Computer
        /// </summary>
        /// <param name="map">Parsed map</param>
        /// <param name="screenWidth">Width of the screen.</param>
        /// <param name="screenHeight">Height of the screen.</param>
        /// <remarks>
        /// Each resize of rendering screen means constructing new computer.
        /// </remarks>
        public RayCastingDistanceComputer(Map map, int screenWidth, int screenHeight)
        {
            _map = map;
            _availableCores = Environment.ProcessorCount > 3 ? Environment.ProcessorCount - 2 : 1;
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
            LastCached = new DistanceResult[_availableCores];
        }

        public bool IsComputing { get; set; }
        public DistanceResult[] LastCached { get; private set; }
        public int ScreenWidth { get; }
        public int ScreenHeight { get; }
        public bool CorrectFishEye { private get; set; } = true;

        /// <summary>
        /// Single time computation entry method
        /// </summary>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        /// <param name="computingMode">Whether to compute single or multi-threaded</param>
        /// <returns>Distance result of the whole screen</returns>
        public DistanceResult Compute(Player player, ComputingMode computingMode)
        {
            switch (computingMode)
            {
                case ComputingMode.MultiThreading:
                    return ComputeMultiThreaded(player);
                case ComputingMode.SingleThreading:
                    return ComputeSingleThreaded(player);
                default:
                    return new DistanceResult(ScreenWidth);
            }
        }

        /// <summary>
        /// Computes the distance of all columns single-threaded.
        /// </summary>
        /// <remarks>
        /// Designed to be run once, not in continuous loop
        /// </remarks>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        /// <returns>Distance result of the whole screen</returns>
        private DistanceResult ComputeSingleThreaded(Player player)
        {
            var result = new DistanceResult(ScreenWidth);
            var inverseWidth = 1d / ScreenWidth;
            if (CorrectFishEye)
            {
                for (int i = 0; i < ScreenWidth; i++)
                {
                    var playerCurrentPosition = player.CurrentPosition;
                    result[i] = GetRayDistanceAndColor(playerCurrentPosition.X, playerCurrentPosition.Y,
                        playerCurrentPosition.DirX + playerCurrentPosition.PlaneX * (inverseWidth * i * 2 - 1),
                        playerCurrentPosition.DirY + playerCurrentPosition.PlaneY * (inverseWidth * i * 2 - 1));
                }
            }
            else
            {
                for (int i = 0; i < ScreenWidth; i++)
                {
                    var playerCurrentPosition = player.CurrentPosition;
                    result[i] = GetRayDistanceAndColorNoFishEyeCorrection(playerCurrentPosition.X,
                        playerCurrentPosition.Y,
                        playerCurrentPosition.DirX + playerCurrentPosition.PlaneX * (inverseWidth * i * 2 - 1),
                        playerCurrentPosition.DirY + playerCurrentPosition.PlaneY * (inverseWidth * i * 2 - 1));
                }
            }

            return result;
        }

        /// <summary>
        /// Computes the distance of all columns multi-threaded.
        /// <remarks>
        /// This method creates new threads and runs them on part of screen
        /// Therefore there is a big overhead in core creation over the single threaded variant
        /// Designed to be run once, not in continuous loop
        /// </remarks>
        /// </summary>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        /// <returns>Distance result of the whole screen</returns>
        private DistanceResult ComputeMultiThreaded(Player player)
        {
            Thread[] threads = new Thread[_availableCores];
            int splitWidth = ScreenWidth / 8;
            DistanceResult[] results = new DistanceResult[_availableCores];

            for (int i = 0; i < threads.Length - 1; i++)
            {
                int x = i;
                threads[i] = new Thread(() =>
                {
                    results[x] = CalculatePartOfScreen(player, x * splitWidth, (x + 1) * splitWidth);
                });
            }

            threads[_availableCores - 1] = new Thread(() =>
            {
                results[_availableCores - 1] =
                    CalculatePartOfScreen(player, (_availableCores - 2) * splitWidth, ScreenWidth);
            });

            foreach (var thread in threads)
            {
                thread.Start();
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }

            return Merge(results);
        }

        /// <summary>
        /// Method for calculating distances to parts of screen
        /// </summary>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        /// <param name="start">Pixel column to start on</param>
        /// <param name="stop">Pixel column to end on</param>
        /// <returns>Distance result of part of screen specified</returns>
        private DistanceResult CalculatePartOfScreen(Player player, int start, int stop)
        {
            var result = new DistanceResult(stop - start);
            var inverseWidth = 1d / ScreenWidth;
            if (CorrectFishEye)
            {
                for (int i = start; i < stop; i++)
                {
                    var playerCurrentPosition = player.CurrentPosition;
                    result[i - start] = GetRayDistanceAndColor(playerCurrentPosition.X, playerCurrentPosition.Y,
                        playerCurrentPosition.DirX + playerCurrentPosition.PlaneX * (inverseWidth * i * 2 - 1),
                        playerCurrentPosition.DirY + playerCurrentPosition.PlaneY * (inverseWidth * i * 2 - 1));
                }
            }
            else
            {
                for (int i = start; i < stop; i++)
                {
                    var playerCurrentPosition = player.CurrentPosition;
                    result[i - start] = GetRayDistanceAndColorNoFishEyeCorrection(playerCurrentPosition.X,
                        playerCurrentPosition.Y,
                        playerCurrentPosition.DirX + playerCurrentPosition.PlaneX * (inverseWidth * i * 2 - 1),
                        playerCurrentPosition.DirY + playerCurrentPosition.PlaneY * (inverseWidth * i * 2 - 1));
                }
            }

            return result;
        }

        /// <summary>
        /// Starts continuous computation through creating tasks with while loops.
        /// <remarks>Number of Tasks created is equal to available cores, which can lead to hangs with other threads</remarks>
        /// </summary>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        public void StartTaskComputing(Player player)
        {
            StartTaskComputing(player, _availableCores);
        }

        /// <summary>
        /// Starts continuous computation through creating tasks with while loops.
        /// <remarks>Number of Tasks created is equal to specified available cores</remarks>
        /// </summary>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        /// <param name="availableCores"></param>
        public void StartTaskComputing(Player player, int availableCores)
        {
            if (_computingState != ComputingState.Idle)
            {
                StopAll();
            }

            IsComputing = true;
            _computingState = ComputingState.Task;
            int splitWidth = ScreenWidth / availableCores;
            Task[] tasks = new Task[availableCores];
            LastCached = new DistanceResult[availableCores];

            for (int i = 0; i < availableCores - 1; i++)
            {
                var x = i;
                tasks[i] = new Task(() =>
                {
                    while (IsComputing)
                    {
                        LastCached[x] = CalculatePartOfScreen(player, x * splitWidth, (x + 1) * splitWidth);
                    }
                });
            }

            tasks[availableCores - 1] = new Task(() =>
            {
                while (IsComputing)
                {
                    LastCached[availableCores - 1] =
                        CalculatePartOfScreen(player, (availableCores - 1) * splitWidth, ScreenWidth);
                }
            });

            foreach (var task in tasks)
            {
                task.Start();
            }
        }

        /// <summary>
        /// Stops running task continuous computation.
        /// </summary>
        public void StopTaskComputing()
        {
            IsComputing = false;
            _computingState = ComputingState.Idle;
            Task.WaitAll();
        }

        /// <summary>
        /// Starts continuous computation through creating one thread with while loop.
        /// <remarks>Number of Threads created is equal to available cores</remarks>
        /// </summary>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        public void StartContinuousComputing(Player player)
        {
            if (_computingState != ComputingState.Idle)
            {
                StopAll();
            }

            IsComputing = true;
            _computingState = ComputingState.Single;
            LastCached = new DistanceResult[1];
            _runningThreads = new Thread[1];

            Thread computingThread = new Thread(() =>
            {
                while (IsComputing)
                {
                    LastCached[0] = CalculatePartOfScreen(player, 0, ScreenWidth);
                }
            });

            _runningThreads[0] = computingThread;

            computingThread.Start();
        }

        /// <summary>
        /// Stops running continuous computation.
        /// </summary>
        public void StopContinuousComputing()
        {
            IsComputing = false;
            _computingState = ComputingState.Idle;
            _runningThreads[0].Join();
        }

        /// <summary>
        /// Starts continuous computation through creating threads with while loops.
        /// <remarks>Number of Threads created is equal to available cores</remarks>
        /// </summary>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        public void StartManualThreadsComputing(Player player)
        {
            StartManualThreadsComputing(player, _availableCores);
        }

        /// <summary>
        /// Starts continuous computation through creating threads with while loops.
        /// <remarks>Number of Threads created is equal to specified amount</remarks>
        /// </summary>
        /// <param name="player">Player as a source of information about the position and look direction.</param>
        /// <param name="availableCores">Specified number of available cores to use</param>
        public void StartManualThreadsComputing(Player player, int availableCores)
        {
            if (_computingState != ComputingState.Idle)
            {
                StopAll();
            }

            IsComputing = true;
            _computingState = ComputingState.Multi;
            _runningThreads = new Thread[availableCores];
            int splitWidth = ScreenWidth / availableCores;
            LastCached = new DistanceResult[availableCores];

            for (int i = 0; i < availableCores - 1; i++)
            {
                int x = i;
                _runningThreads[i] = new Thread(() =>
                {
                    while (IsComputing)
                    {
                        LastCached[x] = CalculatePartOfScreen(player, x * splitWidth, (x + 1) * splitWidth);
                    }
                });
            }

            _runningThreads[availableCores - 1] = new Thread(() =>
            {
                while (IsComputing)
                {
                    LastCached[availableCores - 1] =
                        CalculatePartOfScreen(player, (availableCores - 1) * splitWidth, ScreenWidth);
                }
            });

            foreach (var thread in _runningThreads)
            {
                thread.Start();
            }
        }

        /// <summary>
        /// Stops running thread continuous computation.
        /// </summary>
        public void StopManualThreadsComputing()
        {
            IsComputing = false;
            _computingState = ComputingState.Idle;
            foreach (var thread in _runningThreads)
            {
                thread.Join();
            }
        }

        /// <summary>
        /// Stops all continuous computations
        /// </summary>
        public void StopAll()
        {
            IsComputing = false;

            switch (_computingState)
            {
                case ComputingState.Single:
                    StopContinuousComputing();
                    break;
                case ComputingState.Multi:
                    StopManualThreadsComputing();
                    break;
                case ComputingState.Task:
                    StopTaskComputing();
                    break;
                case ComputingState.Idle:
                    break;
            }
        }

        /// <summary>
        /// This method gets the distance of a ray from the origin coordinates of the player to the coordinates of the first wall hit
        /// in the direction of the ray
        /// Ok i know, this may be very complex and difficult to read and understand, but it is all coupled into one method for the sake
        /// of performance.
        /// </summary>
        /// <param name="originX">Starting X coordinate of the ray</param>
        /// <param name="originY">Starting Y coordinate of the ray</param>
        /// <param name="dirX">Directional X coordinate of the ray</param>
        /// <param name="dirY">Directional Y coordinate of the ray</param>
        /// <returns>Distance between starting position and point of first hit of the ray</returns>
        private DistanceResult.ResultColumn GetRayDistanceAndColor(double originX, double originY, double dirX,
            double dirY)
        {
            #region Variables

            int stepX; // which way are we going on X coordinate
            int stepY; // which way are we going on Y coordinate
            double sideDistX; // distance from next whole number on X coordinate
            double sideDistY; // distance from next whole number on Y coordinate
            double perpendicularWallDist; // final distance we are returning as part of result
            bool isSide = false; // indicates from which side are we hitting next tile
            var deltaDistX =
                Math.Sqrt(1 + (dirY * dirY) / (dirX * dirX)); // distance on X coordinate per 1 on Y coordinate
            var deltaDistY =
                Math.Sqrt(1 + (dirX * dirX) / (dirY * dirY)); // distance on Y coordinate per 1 on X coordinate
            var absX = (int) originX; // X coordinate of tile we are hitting next 
            var absY = (int) originY; // Y coordinate of tile we are hitting next

            #endregion

            #region Direction detection

            if (dirX < 0)
            {
                stepX = -1;
                sideDistX = (originX - absX) * deltaDistX;
            }
            else
            {
                stepX = 1;
                sideDistX = (absX + 1 - originX) * deltaDistX;
            }

            if (dirY < 0)
            {
                stepY = -1;
                sideDistY = (originY - absY) * deltaDistY;
            }
            else
            {
                stepY = 1;
                sideDistY = (absY + 1 - originY) * deltaDistY;
            }

            #endregion

            #region Stepping until hit solid wall

            while (_map[absX, absY].Empty)
            {
                if (sideDistX < sideDistY)
                {
                    sideDistX += deltaDistX;
                    absX += stepX;
                    isSide = true;
                }
                else
                {
                    sideDistY += deltaDistY;
                    absY += stepY;
                    isSide = false;
                }
            }

            #endregion

            #region Distance calculation

            System.Drawing.Pen actualPen;
            if (isSide)
            {
                // ReSharper disable once PossibleLossOfFraction
                // Since stepX can be only 1 or -1, we don't lose fraction, this is intended
                perpendicularWallDist = (absX - originX + (1 - stepX) / 2) / dirX;
                actualPen = _map[absX, absY].SidePen;
            }
            else
            {
                // ReSharper disable once PossibleLossOfFraction
                // Since stepY can be only 1 or -1, we don't lose fraction, this is intended
                perpendicularWallDist = (absY - originY + (1 - stepY) / 2) / dirY;
                actualPen = _map[absX, absY].Pen;
            }

            #endregion

            return new DistanceResult.ResultColumn(actualPen, perpendicularWallDist);
        }

        /// <summary>
        /// This method gets the distance of a ray from the origin coordinates of the player to the coordinates of the first wall hit
        /// in the direction of the ray
        /// Ok i know, this may be very complex and difficult to read and understand, but it is all coupled into one method for the sake
        /// of performance.
        /// </summary>
        /// <remarks>
        /// This method calculates the real distance without fish eye correction.
        /// </remarks>
        /// <param name="originX">Starting X coordinate of the ray</param>
        /// <param name="originY">Starting Y coordinate of the ray</param>
        /// <param name="dirX">Directional X coordinate of the ray</param>
        /// <param name="dirY">Directional Y coordinate of the ray</param>
        /// <returns>Distance between starting position and point of first hit of the ray</returns>
        private DistanceResult.ResultColumn GetRayDistanceAndColorNoFishEyeCorrection(double originX, double originY,
            double dirX, double dirY)
        {
            #region Variables

            int stepX; // which way are we going on X coordinate
            int stepY; // which way are we going on Y coordinate
            double sideDistX; // distance from next whole number on X coordinate
            double sideDistY; // distance from next whole number on Y coordinate
            double perpendicularWallDist; // final distance we are returning as part of result
            bool isSide = false; // indicates from which side are we hitting next tile
            var deltaDistX =
                Math.Sqrt(1 + (dirY * dirY) / (dirX * dirX)); // distance on X coordinate per 1 on Y coordinate
            var deltaDistY =
                Math.Sqrt(1 + (dirX * dirX) / (dirY * dirY)); // distance on Y coordinate per 1 on X coordinate
            var absX = (int) originX; // X coordinate of tile we are hitting next 
            var absY = (int) originY; // Y coordinate of tile we are hitting next

            #endregion

            #region Direction detection

            if (dirX < 0)
            {
                stepX = -1;
                sideDistX = (originX - absX) * deltaDistX;
            }
            else
            {
                stepX = 1;
                sideDistX = (absX + 1 - originX) * deltaDistX;
            }

            if (dirY < 0)
            {
                stepY = -1;
                sideDistY = (originY - absY) * deltaDistY;
            }
            else
            {
                stepY = 1;
                sideDistY = (absY + 1 - originY) * deltaDistY;
            }

            #endregion

            #region Stepping until hit solid wall

            while (_map[absX, absY].Empty)
            {
                if (sideDistX < sideDistY)
                {
                    sideDistX += deltaDistX;
                    absX += stepX;
                    isSide = true;
                }
                else
                {
                    sideDistY += deltaDistY;
                    absY += stepY;
                    isSide = false;
                }
            }

            #endregion

            #region Distance calculation

            System.Drawing.Pen actualPen;
            double actX;
            double actY;
            if (isSide)
            {
                // ReSharper disable once PossibleLossOfFraction
                // Since stepX can be only 1 or -1, we don't lose fraction, this is intended
                perpendicularWallDist = absX - originX + (1 - stepX) / 2;
                actX = perpendicularWallDist;
                actY = perpendicularWallDist * (dirY / dirX);
                actualPen = _map[absX, absY].SidePen;
            }
            else
            {
                // ReSharper disable once PossibleLossOfFraction
                // Since stepY can be only 1 or -1, we don't lose fraction, this is intended
                perpendicularWallDist = absY - originY + (1 - stepY) / 2;
                actY = perpendicularWallDist;
                actX = perpendicularWallDist * (dirX / dirY);
                actualPen = _map[absX, absY].Pen;
            }

            #endregion

            return new DistanceResult.ResultColumn(actualPen, Math.Sqrt((actX) * (actX) + (actY) * (actY)));
        }

        /// <summary>
        /// Merges array of DistanceResults to one DistanceResult with equal columns
        /// </summary>
        /// <remarks>
        /// This method is O(n).
        /// </remarks>
        /// <param name="results">Enumerable of DistanceResults</param>
        /// <returns>Merged DistanceResult</returns>
        private DistanceResult Merge(IEnumerable<DistanceResult> results)
        {
            var rows = results.Select(x => x.Results);
            DistanceResult result = new DistanceResult(ScreenWidth);
            List<DistanceResult.ResultColumn> columns = new List<DistanceResult.ResultColumn>();

            foreach (var resultColumn in rows)
            {
                columns.AddRange(resultColumn);
            }

            result.Results = columns.ToArray();
            return result;
        }
    }

    /// <summary>
    /// Enum to choose the computation mode for single distance result.
    /// </summary>
    public enum ComputingMode
    {
        MultiThreading,
        SingleThreading
    }

    /// <summary>
    /// Enum to keep the current state of distance computer.
    /// </summary>
    internal enum ComputingState
    {
        Single,
        Multi,
        Task,
        Idle
    }
}

﻿namespace GeometryExtended
{
    using System;
    using System.Drawing;
    using System.IO;
    using DataStructures;

    public class Parser
    {
        /// <summary>
        /// Reads map from a file with following format:
        /// First line has xSize and ySize split by a space
        /// ySize rows, xSize columns where a cell is either 0 for empty space or ColorName
        /// <example>https://docs.microsoft.com/en-us/dotnet/api/system.drawing.knowncolor?view=netframework-4.7.2</example>
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static Map ReadMap(string fileName)
        {
            using (StreamReader file = new StreamReader(fileName))
            {
                try
                {
                    string[] sizes = file.ReadLine()?.Split();
                    if (sizes == null)
                    {
                        throw new System.Data.DataException("File hasn't correct format, sizes not found");
                    }

                    int ySize = int.Parse(sizes[1]);
                    int xSize = int.Parse(sizes[0]);
                    Tile[,] layout = new Tile[xSize, ySize];

                    for (int row = 0; row < ySize; row++)
                    {
                        string[] line = file.ReadLine()?.Split();
                        if (line == null)
                        {
                            throw new System.Data.DataException("File hasn't correct format, line not found");
                        }

                        for (int column = 0; column < xSize; column++)
                        {
                            Color color = line[column] == "0" ? Color.Empty : Color.FromName(line[column]);
                            Color darker = GetDarker(color);
                            layout[column, row] = new Tile
                            {
                                Empty = line[column] == "0",
                                Pen = line[column] == "0" ? null : new Pen(color),
                                SidePen = line[column] == "0" ? null : new Pen(darker)
                            };
                        }
                    }

                    return new Map(layout, xSize, ySize);
                }
                catch (ArgumentOutOfRangeException e)
                {
                    throw new ArgumentOutOfRangeException("One of the lines was not in correct format", e);
                }
            }
        }

        /// <summary>
        /// Tries to create a darker version of the color given
        /// </summary>
        /// <param name="color">Original color</param>
        /// <returns>Darker version of original color, if possible</returns>
        private static Color GetDarker(Color color)
        {
            var red = color.R > 30 ? color.R - 30 : 0;
            var green = color.G > 30 ? color.G - 30 : 0;
            var blue = color.B > 30 ? color.B - 30 : 0;

            return Color.FromArgb(red, green, blue);
        }
    }
}
